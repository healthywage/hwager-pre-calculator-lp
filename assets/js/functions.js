// sections to show or hide
const winner5000Container = document.getElementById('winners-5000');
const winner3000Container = document.getElementById('winners-3000');
const winner1000Container = document.getElementById('winners-1000');
const winner500Container = document.getElementById('winners-500');
const recentWinnersContainer = document.getElementById('recent-winners-container');
const biggestWeightLossContainer = document.getElementById('biggest-weight-loss-container');
const lowCarbWinnersContainer = document.getElementById('low-carb-winners-container');

// links to click
const winner5000 = {element: document.getElementById('winner-5000'), container: winner5000Container};
const winner3000 = {element: document.getElementById('winner-3000'), container: winner3000Container};
const winner1000 = {element: document.getElementById('winner-1000'), container: winner1000Container};
const winner500 = {element: document.getElementById('winner-500'), container: winner500Container};
const recentWinners = {element: document.getElementById('recent-winners'), container: recentWinnersContainer};
const biggestWeightLoss = {element: document.getElementById('biggest-weight-loss'), container: biggestWeightLossContainer};
const lowCarbWinners = {element: document.getElementById('low-carb-winners'), container: lowCarbWinnersContainer};


const clickableElements = [winner5000, winner3000, winner1000, winner500, recentWinners, biggestWeightLoss, lowCarbWinners];
const containers = [winner5000Container, winner3000Container, winner1000Container, winner500Container, recentWinnersContainer, biggestWeightLossContainer, lowCarbWinnersContainer]


clickableElements.forEach(function(elemContainer) {
    elemContainer.element.addEventListener('click', function(event) {

        const elementsToHide = containers.filter(function(e2) { return elemContainer.container !== e2 })

        elementsToHide.forEach(function(toHide) {
            toHide.parentElement.classList.add('hidden');
        });

        elemContainer.container.parentElement.classList.remove('hidden');
    })
})
