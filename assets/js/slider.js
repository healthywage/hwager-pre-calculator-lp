var gallery = document.getElementById('slider');

var minLeft = 0,
    minCenter = 1,
    minRight = 2,
    minCanMove = true;

function changeIdx(arr, direction) {

    var length = arr.childElementCount;

    if (!minCanMove) return;
    minCanMove = false;
    setTimeout(function(){minCanMove = true}, 1000)
    if (length == 3) return;

    outLeft = minLeft;
    outRight = minRight;
    left = setCenter(minLeft, length, direction, 'left');
    center = setCenter(minCenter, length, direction, 'center');
    right = setCenter(minRight, length, direction, 'right');

    if (direction == 'next') {

        for (var i = 0; i < length; i++) {

            switch (i) {
                case outLeft:
                    arr.children[i].className = 'case-wrapper to-out-left';
                    break;
                case left:
                    arr.children[i].className = 'case-wrapper to-left';
                    break;
                case center:
                    arr.children[i].className = 'case-wrapper from-right';
                    break;
                case right:
                    arr.children[i].className = 'case-wrapper from-out-right';
                    break;

                default:
                    arr.children[i].className = 'case-wrapper out';
                    break;
            }

        }

    }

    if (direction == 'prev') {
        for (var i = 0; i < length; i++) {

            switch (i) {
                case left:
                    arr.children[i].className = 'case-wrapper from-out-left';
                    break;
                case center:
                    arr.children[i].className = 'case-wrapper from-left';
                    break;
                case right:
                    arr.children[i].className = 'case-wrapper to-right';
                    break;
                case outRight:
                    arr.children[i].className = 'case-wrapper to-out-right';
                    break;
                default:
                    arr.children[i].className = 'case-wrapper out';
                    break;
            }

        }

    }
}

function minToLeft() {
    changeIdx(gallery, 'prev');
}
function minToRight() {
    changeIdx(gallery, 'next');
}


function setCenter(idx, limit, dir, pos) {

    var num = idx;

    if (dir == 'next') {
        num++
        if (num >= limit) num = 0;
    }

    if (dir == 'prev') {
        num--;
        if (num < 0) {
            num = limit - 1;
        }

    }

    switch (pos) {
        case 'out-left':
            toOutLeft = num;
            break;
        case 'left':
            minLeft = num;
            break;
        case 'center':
            minCenter = num;
            break;
        case 'right':
            minRight = num;
            break;
        default:
            break;
    }

    return num;

}
