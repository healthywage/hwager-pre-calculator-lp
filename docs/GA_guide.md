## What did we do?
We did:
- the complete structure of the LP
- version mobile
- version desktop
- almost all animations and effects
- test using Google Optimize
- enhance the way to import the code to Google Optimize

## What didn't we do yet?
- "recent winners" animation
- find and upload the right images for some cards. Now we're using defaults

Because of time we couldn't finish the "pending changes".


## How to upload the code to Google Optimize?

- Go to Google Optimize project
- Click and delete all elements. **Do not** remove `body`
- Click on the tag "body" on the top of the site and click on "edit element" on the box on the right side
- Click on "edit element" on the box on the right side
- Click on "Insert HTML"
- Paste:
```
  <section id="inject-here"></section>
```
- Click on the tag "body" on the top of the site and click on "edit element" on the box on the right side
- Click on "Run Javascript"
- Paste:

```
fetch('https://bitbucket.org/healthywage/hwager-pre-calculator-lp/raw/c4717c87079f7f69ae8ba800dbbec2f41c367c36/index.html', {
	headers: {
      'Content-Type': 'text/html',
    },
})
.then(function(response) {
  response.text().then(function(data) {
    document.getElementById('inject-here').innerHTML = data;

    const fns = document.createElement('script');
    fns.setAttribute('src','https://thewinningskinny.com/wp-content/static/hwager-precalculator-lp/js/functions.js');
    document.head.appendChild(fns);

    const slider = document.createElement('script');
    slider.setAttribute('src','https://thewinningskinny.com/wp-content/static/hwager-precalculator-lp/js/slider.js');
    document.head.appendChild(slider);
    
    const sheet = document.getElementById("thb-style-css");
    sheet.disabled = true;
    sheet.parentNode.removeChild(sheet);
    
    
    const sheet2 = document.getElementById("thb-app-css");
    sheet2.disabled = true;
    sheet2.parentNode.removeChild(sheet2);
    
    
    const scrollToTopElement = document.getElementById("scroll_totop");
    scrollToTopElement.disabled = true;
    scrollToTopElement.parentNode.removeChild(scrollToTopElement);
  });
})

```

- Save
